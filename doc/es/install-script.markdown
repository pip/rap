% RAP-INSTALL-SCRIPT(1) Manual de RAP | RAP
% fauno <fauno@endefensadelsl.org>
% 2013

# NOMBRE

rap install-script instala script que se ejecutan en eventos de la VPN.


# SINOPSIS

rap install-script [-hves] nodo-local evento script


# DESCRIPCION

Instala scripts que se ejecutan durante eventos de la VPN.  Al momento
`tincd` sólo soporta dos tipos de eventos luego de iniciado, cuando
un nodo se conecta o desconecta, o cuando una subred es anunciada o
desanunciada.


# OPCIONES

-h
:    Este mensaje

-v
:    Modo verborrágico

-e
:    Listar los eventos disponibles

-s
:    Listar los scripts disponibles


# EVENTOS

tinc
:    Este evento se ejecuta cuando se inicia o se detiene la VPN.
     Colocar scripts que sólo deben ejecutarse una vez acá.

host
:    Ejecutar este script cada vez que un nodo aparece o desaparece.

subnet
:    Ejecutar este script cada vez que una subred aparece o desaparece.


# SCRIPTS

debug
:    Útil para debuguear eventos

ipv6-router
:    Configura este nodo como un router/gateway de IPv6

ipv6-default-route
:    Configura la ruta IPv6 por defecto para este nodo

port-forwarding
:    Le pide al router que abra los puertos, usando NAT-PMP y UPnP


# EJEMPLOS

## Configurar el nodo como un router IPv6

_rap install-script_ ponape host ipv6-router


# VER TAMBIEN

_tinc.conf(5)_
