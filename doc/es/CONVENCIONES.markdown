% CONVENCIONES(2) Manual de RAP | rap
% fauno <fauno@endefensadelsl.org>
% 2013

# NOMBRE

Notas para desarrolladorxs de RAP :)


# SINOPSIS

lib/
:    herramientas

lib/exec/
:    comandos

skel/
:    archivos base para tinc

skel/scripts/
:    directorio de scripts/hooks

doc/
:    documentación

hosts/
:    archivos de nodos

nodos/
:    nodos propios

locale/
:    traducciones


# DESCRIPCION

## Dónde van los scripts

El script _rap_ autodescubre los comandos siguiendo la convención
_lib/exec/el-script_.  Además configura algunas variables de entorno que
los scripts pueden usar para saber en qué red están trabajando.

Los scripts pueden estar en cualquier lenguaje de programación mientras
sepan leer esas variables de entorno.


## Variables de entorno

Estas son las variables de entorno que _rap_ exporta para el resto de
los comandos.

TINC
:    Ubicación del directorio del nodo, por defecto _/etc/tinc/rap_.

NETWORK
:    Nombre de la red, por defecto _rap_.

RAP
:    Path completo de _rap_.

RAP\_DIR
:    Path completo del directorio de trabajo, por defecto el directorio
     base de _RAP_

RAP\_LIBDIR
:    Path completo del directorio de comandos.

RAP\_HOSTS
:    Path completo del directorio de hosts.

KEYSIZE
:    Tamaño por defecto de las llaves.

TINCD\_FLAGS
:    Flags para el demonio _tincd_.

PORT
:    Puerto por defecto

sudo
:    Usar esta variable delante de cualquier comando que deba correr con
     privilegios de root, ej: _${sudo} rsync_.  Requiere _"root=true"_ al
     principio del script.


## Dónde va la documentación

La documentación lleva el nombre completo del script:
_doc/idioma/tuscript.markdown_.  La función _help()_ en _lib/msg_ lo
lleva como argumento para mostrar la ayuda.

Además, toma la variable de entorno _PAGER_ para paginar la salida, por
defecto se usa _less_.


## Flags y parámetros

_rap comando_ -flags nodolocal parametros extra

Seguido de _rap_ viene el comando, al que se le pasan en orden las
opciones (con sus valores).  El primer parámetro siempre tiene que ser
el nodo local en el que se realiza la acción.  Luego vienen los
parámetros extra (nombres de otros nodos, por ejemplo).

## Funciones comunes

En el archivo _lib/common_ se almacenan las funciones de uso común entre
todos los comandos.  Se la puede incluir en un script añadiendo la línea

    . "${RAP_LIBDIR}"/common

al principio del script.

**Nota:** Agregar _root=true_ antes de common para poder correr
funciones de root.

### Variables

* self: nombre del script. Usado para obtener el nombre del
  script. Ejemplo: _help $self_ llama a la ayuda del script actual.

### Funciones

* _add\_to\_file()_: Agrega una línea al final de un archivo. Uso:
  _add\_to\_file archivo "Texto a agregar"_

* _requires()_: Indica que el script necesita que un programa se
  encuentre en el PATH.  Se recomienda cuando el script llama a un
  programa que puede no encontrarse en una instalación estándar.  Uso:
  _requires avahi-publish rsync_

* _get\_node\_dir()_: Encuentra el directorio de un nodo pasándole el
  nombre del nodo como argumento.  _node\_dir="$(get\_node\_dir
  ${node})"_

* _get\_node\_file()_: Encuentra el archivo de host de un nodo dentro
  del directorio del nodo.  _node\_file="$(get\_node\_file ${node})"_

* _get\_node\_name()_: Limpia el nombre del nodo de caracteres inválidos

* _get\_host\_file()_: Obtiene el archivo del nodo en $RAP\_HOSTS


## Mensajes

En _lib/msg_ se encuentran las funciones básicas para imprimir mensajes
en la salida de errores estándar.  Esto es para que no sean procesados
como la salida estándar de los scripts, que se reservan para poder
enviar la información a una tubería.

No es necesario incluirla ya que se llama desde _lib/common_.

Todas las funciones tienen soporte para traducciones utilizando gettext,
por lo que los mensajes que se completan con variables deben seguir el
formato de _printf_: _%s_ para reemplazar por cadenas, _%d_ para números
enteros, etc.

Por ejemplo: _msg "Procesando el nodo %s..." "$node"_

* _msg()_: Información
* _error()_: Mensaje de error
* _warning()_: Alerta
* _fatal\_error()_: Imprime un mensaje de error y termina el programa
  inmediatamente
* _tip()_: Recomendaciones, por ejemplo, cual comando correr a continuación.


## Los comandos

La mayoria de los comandos solo configuran, luego hay que enviar los
cambios a directorio de instalación con el comando _rap init install_.
