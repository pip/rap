% RAP-INIT(1) Manual de RAP | rap
% fauno <fauno@endefensadelsl.org>
% 2013

# NOMBRE

Crea un nuevo nodo


# SINOPSIS

rap init [-f] [-q] [-p 655] [-a dominio.eninternet.tld] [-c otronodo] nodo


# OPCIONES

-h
:    Ayuda

-q
:    Modo silencioso

-a ejemplo.org
:    Ubicación pública del nodo (dominio o IP).  Puede especificarse
     varias veces en el orden en que se quiera que los demás nodos
     intenten conectarse.  Si no se especifica esta opción se entiende
     que el nodo no es accesible públicamente y no acepta conexiones
     (sólo cliente).

-c nodo-remoto
:    Conectar a este nodo.  Versión rápida de _rap connectto_.  Puede
     especificarse varias veces.

-i
:    Instalar al finalizar (requiere root).  Es lo mismo que correr
     _rap install_ más tarde.

-f
:    Forzar la creación de un nodo.  Útil si se canceló previamente o se
     quiere comenzar desde cero.

-p 655
:    Número de puerto, por defecto 655 o la variable de entorno
     _RAP\_PORT_.

# DESCRIPCION

Genera la configuración básica de un nodo y lo almacena en el directorio
_nodos/nombre-del-nodo_.

Se puede correr varias veces con diferentes nombres de nodo para tener
configuraciones separadas (un sólo host, varios nodos) o unidas (un nodo
para cada host local o remoto).

Por ejemplo, los dispositivos embebidos pueden no soportar las
herramientas de _rap_, pero desde un sistema GNU/Linux se puede generar
la configuración y luego copiarla al host que corresponda (al celular o
tablet Android, router con OpenWrt, etc.)

_IMPORTANTE_: La configuración por defecto de un nodo sin el campo
Address asume que se encuentra detrás de un firewall o que no están
configurados para aceptar conexiones directas.  Si luego agregás una
dirección pública también tenés que deshabilitar la opción IndirectData.


# EJEMPLOS

## Uso básico con una sola conexión

  rap init -c trululu guachiguau

## Crear un nodo público con una conexión e instalarlo localmente

  rap init -i -a guachiguau.org -c trululu guachiguau
