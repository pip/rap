.DEFAULT_GOAL := all

src_man = $(wildcard doc/*/*.markdown)
out_man = $(patsubst %.markdown,%.1,$(src_man))

all: man
clean: man-clean

man: $(out_man)
man-clean:
	rm -rf $(out_man)

%.1: %.markdown
	pandoc --standalone \
	       --output='$@' \
	       --to=man \
	       '$<'
