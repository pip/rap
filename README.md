# Red Autónoma Pirata (RAP)

> Un fork de LibreVPN, una red libre virtual.

Estos son los scripts de configuración y administración de la RAP
y aplican muchas de las acciones comunes sobre tinc.

Para obtener ayuda, ejecutar `./rap -h`.  Cada comando tiene su -h
también.

## Crear un nodo

    rap init -h

## Dónde está la configuración de mi nodo?

En el directorio `nodes/`, esto permite mantener la configuración de
varios nodos en una sola máquina, intercambiarlos o copiarlos a otras
máquinas.

Por cada cambio en la configuración tenés que usar `rap install` o
`rap push` (dependiendo si es un nodo local o remoto).

## El nombre de mi nodo está lleno de guiones bajos / no me gusta

Si no le diste un nombre a tu nodo, `rap` va a usar el nombre de tu
máquina.  Si cuando instalaste tu distro dejaste que el instalador elija
por su cuenta, vas a tener un nombre de nodo lleno de guiones bajos en
lugar de los caracteres que tinc no acepta (por ejemplo, "-" se
convierte en "\_") y además el modelo de tu computadora.

Si querés cambiar esto lo mejor es cambiando el "hostname" de tu
computadora, siguiendo los pasos que indique tu distro.  Sino, agregá un
nombre al comando `./rap init un_nombre_lindo`.

## Cómo me conecto a la red?

Esta es una red de confianza, podés crear e intercambiar nodos con tu
grupx de afinidad ;)

## Requisitos

`tinc`, `bash`.

Además los scripts informan sobre otros comandos que pueden llegar a
necesitar.

## Desarrolladoras

Ver _doc/CONVENCIONES.markdown_.
